require "./datos.rb"
require "./bar.rb"

class EasyBar
  attr_accessor :datos

  def initialize(db)
    @datos = db
  end

  def buscar_bar_por_nombre(bar)
    @datos.lista.select {|p| p.nombre == bar}
   end

  def agregar_comentario(bar,comentario)
    if comentario == ""
      mensaje = "Comentario Invalido"
    else
      buscar_bar_por_nombre(bar.nombre)[0].comentarios << comentario
      mensaje = "Comentario Enviado"
    end
    return mensaje
  end
end
