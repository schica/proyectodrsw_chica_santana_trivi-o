Readme EasyBar

#Para ejecutar el proyecto

-Antes de ejecutar
  -Desde consola ingrese a la carpeta donde se encuentra el archivo easyBar.rb
  -Ejecute el comando ruby easyBar.rb

#Para ejecutar las pruebas

-Antes de ejecutar
  -Desde consola ingrese a la carpeta donde se encuentra el archivo easyBar_rspec.rb
  -Ejecute el comando rspec easyBar_rspec.rb

-Durante la ejecución
  -Debe mostrarse las 3 pruebas que posee el archivo en ejecución.
  -En los resultados de las 3 pruebas todas deben aparecer como pasadas, es decir, no deben aparecer errores.
