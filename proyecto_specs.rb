require 'rspec'

class EasyBar

  attr_accessor :lista

  def initialize (lista)
    self.lista = lista
  end

  def buscarBar(bar)
    return lista
  end

  def verDescripcionBar(bar)
    return "El bar es bonito"
  end

  def buscarBarZonas(lista)
    return lista
  end

end

describe "Historias de usuario de EasyBar" do
  describe "Pasos para buscar un bar" do
    context "cuando el usuario escribe la palabra Chamois en el buscador" do
      it "debe mostrarle la lista de los bares que contienen este nombre" do
	bar = EasyBar.new(["Chamois","Poropompero"])
        barLista = bar.buscarBar("Chamois")
        expect(barLista.length > 0).to eq true
      end
      context "y selecciona el bar Chamois" do
        it "debe mostrarle la descripción del bar Chamois" do
	  bar = EasyBar.new(["Chamois","Poropompero"])
	  barDescripcion = bar.verDescripcionBar("Chamois")
	  expect(barDescripcion!=nil).to eq true
        end
      end
    end
    context "cuando el usuario selecciona una busqueda por zonas" do
      context "y selecciona la zona de chapinero" do
        it "debe mostrar la lista de los bares que se encuentran en chapinero ordenada por mayor calificación" do
	  bar = EasyBar.new(["Chamois","Porompopero"])
	  barLista = bar.buscarBarZonas("Chapinero")
	  expect(barLista.length > 0).to eq true
	end
      end
    end
  end
  describe "Pasos para hacer un comentario de un bar" do
    context "cuando se selecciona el bar chamois" do
      context "y se selecciona la opción escribir comentario" do
        context "y se escribe el comentario y se selecciona la opción de comentar" do
          it "debe aparecer un mensaje indicando que el comentario fue recibido" do
 	    expect(true).to eq true
          end
        end
      end
    end
  end
  describe "Pasos para calificar un bar" do
    context "cuando se selecciona el bar chamois" do
      context "y cuando se da una calificación de 5 estrellas" do
        it "debe aparecer un mensaje indicando que la calificación fue recibida" do
 	  expect(true).to eq true
        end
      end
    end
  end
  describe "Pasos para recomendar un nuevo bar" do
    context "cuando el usuario selecciona la opcion de recomendar un nuevo bar" do
      context "e ingresa los datos solicitados" do
        it "debe aparecer un mensaje indicando que la solicitud fue enviada y que el adminitrador verificara los datos" do
          expect(true).to eq true
        end
      end
    end
  end
  describe "Pasos para compartir un bar por una red social" do
    context "cuando el usuario desea compartir un bar por facebook" do
      context "y selecciona el icono de facebook" do
        it "debe abrirle una ventana donde le muestra lo que compartirá" do
          expect(true).to eq true
	end
      end
    end
  end
end
