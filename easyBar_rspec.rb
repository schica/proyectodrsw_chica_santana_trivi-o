require 'rspec'
require './easyBar.rb'
require './datos.rb'
require './bar.rb'

describe "Reglas EasyBar" do
  describe "Pasos para buscar un bar por nombre" do
    context "Cuando el usuario busca Chamois" do
      it "debería aparecer la información del bar Chamois" do
	      easy_bar = EasyBar.new(Datos_EasyBar.new)
	      bar = Bar.new("Chamois")
        expect(easy_bar.buscar_bar_por_nombre(bar.nombre)[0]) == bar
      end
    end

    context "Cuando el usuario busca Poropompero" do
      it "debería aparecer la información del bar Poropompero" do
	      easy_bar = EasyBar.new(Datos_EasyBar.new)
   	    bar = Bar.new("Poropompero")
        expect(easy_bar.buscar_bar_por_nombre(bar.nombre)[0]) == bar
      end
    end

    context "Cuando el usuario busca Simona" do
      it "debería aparecer que el bar no está registrado" do
	      easy_bar = EasyBar.new(Datos_EasyBar.new)
	      bar = Bar.new("Simona")
        expect(easy_bar.buscar_bar_por_nombre(bar.nombre)[0]) == nil
      end
    end
  end
  describe "Pasos para comentar un bar" do
    context "Cuando el usuario selecciona el bar Chamois" do
      context "Selecciona la opcion escribir comentario" do
        context "Escribe un comentario y selecciona enviar" do
          it "Deberia aparecer un mensaje de Comentario Enviado" do
            easy_bar = EasyBar.new(Datos_EasyBar.new)
            bar = Bar.new("Chamois")
            expect(easy_bar.agregar_comentario(bar,"Que buen bar ;)")).to eq "Comentario Enviado"
          end
        end
        context "No escribe ningun comentario y selecciona enviar" do
          it "Deberia aparecer un mensaje de Comentario Invalido" do
            easy_bar = EasyBar.new(Datos_EasyBar.new)
            bar = Bar.new("Chamios")
            expect(easy_bar.agregar_comentario(bar,"")).to eq "Comentario Invalido"
          end
        end
      end
    end
  end
end

